﻿app.controller('AccessoryModalCtrl', ["$scope", "CrudService", "$uibModalInstance", "item", "option", "toastr",
    function ($scope, crudService, $uibModalInstance, item, option, $notifications) {

        var baseUrl = '/api/accessories/';
        $scope.Accessory = item;

        var getProviders = function () {
            var apiRoute = '/api/providers/';
            var promise = crudService.getAll(apiRoute);
            promise.then(function (response) {
                    $scope.PROVIDERS = response.data.Data;
                },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        var addAccessory = function () {
            var apiRoute = baseUrl;
            var postData = $scope.Accessory;
            var promise = crudService.post(apiRoute, postData);
            promise.then(function (response) {
                var dataResult = response.data;
                $uibModalInstance.close(dataResult);
            },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;
        }


        var updateAccessory = function () {
            var apiRoute = baseUrl + "/" + $scope.Accessory.Id;
            var postData = $scope.Accessory;
            var promise = crudService.put(apiRoute, postData);
            promise.then(function (response) {
                var dataResult = response.data;
                $uibModalInstance.close(dataResult);
            },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;
        }

        $scope.save = function () {
            if (option[0].Type === 'add') {
                addAccessory();
            }
            if (option[0].Type === 'edit') {
                updateAccessory();
            }
        }


        $scope.cancel = function () {
            $uibModalInstance.dismiss("Cancel");
            console.log("hihi");
        }

        const initMain = function () {

            if (option[0].Type === 'edit') {
                $scope.Title = 'CẬP NHẬT VẬT TƯ';
            } else if (option[0].Type === 'add') {
                $scope.Title = 'THÊM MỚI VẬT TƯ';
            }

            getProviders();

        };

        initMain();

    }]);