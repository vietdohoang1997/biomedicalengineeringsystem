﻿app.controller('DeviceCtrl', ['$scope', 'CrudService', '$uibModal', "toastr",
    function ($scope, crudService, $uibModal, $notifications) {
        // Base Url 
        var baseUrl = '/api/devices/';
        $scope.deviceID = 0;

        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page.  

        $scope.page = 0;

        $scope.getStatus = function (id) {
            if (id === 0) {
                return "Mới 100%";
            }
            if (id === 1) {
                return "Đang sử dụng";
            }

            if (id === 2) {
                return "Hỏng";
            }

            if (id === 3) {
                return "Đang sửa";
            }

            if (id === 4) {
                return "Đã sửa";
            }

            if (id === 5)
                return "Chờ thanh lý";
        };
            


        var getDevices = function () {
            var apiRoute = baseUrl + "paging?pageIndex=" + ($scope.pageIndex - 1) + "&pageSize=" + $scope.pageSizeSelected;
            var promise = crudService.getAllPaging(apiRoute);
            promise.then(function (response) {
                $scope.devices = response.data;
                $scope.totalCount = response.data.TotalCount;
                $scope.numPages = Math.ceil($scope.totalCount / $scope.pageSizeSelected);
            },
                function (error) {
                    console.log("Error: " + error);
                });
        };

        var getDepartments = function () {
            var apiRoute = '/api/departments/';
            var promise = crudService.getAll(apiRoute);
            promise.then(function (response) {
                $scope.departments = response.data.Data;
            },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        function findObjectByKey(array, key, value) {
            for (var i = 0; i < array.length; i++) {
                if (array[i][key] === value) {
                    return array[i];
                }
            }
            return null;
        };


        $scope.getDepartmentName = function (id) {
            var result = findObjectByKey($scope.departments, 'Id', id);
            return result.Name;
        };


        var getProviders = function () {
            var apiRoute = '/api/providers/';
            var promise = crudService.getAll(apiRoute);
            promise.then(function (response) {
                $scope.providers = response.data.Data;
            },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        $scope.pageChanged = function () {
            getDevices();
        };

        $scope.changePageSize = function () {
            $scope.pageIndex = 1;
            getDevices();
        };

        var initData = function () {
            getDevices();
            getDepartments();
            getProviders();
        }

        $scope.Add = function () {
            var modalInstance = $uibModal.open({
                templateUrl: "Front-End/app-data/views/device/device-modal.html",
                controller: "DeviceModalCtrl",
                size: "lg",
                resolve: {
                    item: function () {
                        var obj = {};
                        return obj;
                    },
                    option: function () {
                        return [{ Type: "add" }];
                    }
                }
            });
            modalInstance.result.then(
                function (response) {
                    if (response.Code === 1) {
                        $notifications.success("Thêm mới thành công", "Thông báo");
                        initData();
                    }
                },
                function () {
                    // $log.info('Modal dismissed at: ' + new Date());
                }
            );
        };

        $scope.Update = function (item) {
            var modalInstance = $uibModal.open({
                templateUrl: "Front-End/app-data/views/device/device-modal.html",
                controller: "DeviceModalCtrl",
                size: "lg",
                keyboard: false,
                backdrop: "static",
                resolve: {
                    item: function () {
                        return item;
                    },
                    option: function () {
                        return [{ Type: "edit" }];
                    }
                }
            });

            modalInstance.result.then(
                function (response) {
                    if (response.Code === 1) {
                        $notifications.success("Cập nhật thành công");
                    } else {
                        $notifications.warning("Có lỗi xảy ra");
                    }
                    $scope.Button.Delete.Visible = false;
                    initData();
                },
                function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                }
            );
        };

        $scope.Delete = function (item) {
            var apiRoute = baseUrl + "/" + item.Id;
            var promise = crudService.delete(apiRoute);
            promise.then(function (response) {
                var dataResult = response.data;
                if (dataResult.Code === 1) {
                    initData();
                    $notifications.success("Đã xóa",
                        "Thông báo",
                        {
                            timeOut: 5000
                        });
                } else {
                    $notifications.warning("Không xóa được",
                        "Thông báo",
                        {
                            timeOut: 5000
                        });
                }
            },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;

        }

        $scope.Search = function () {
            var apiRoute = baseUrl + "paging?pageIndex=" + ($scope.pageIndex - 1) + "&pageSize=" + $scope.pageSizeSelected + "&textSearch=" + $scope.textSearch;
            var promise = crudService.getAllPaging(apiRoute);
            promise.then(function (response) {
                $scope.devices = response.data;
                $scope.totalCount = response.data.DataCount;
                $scope.numPages = Math.ceil($scope.totalCount / $scope.pageSizeSelected);
            },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        $scope.Notification = function (item) {
            var modalInstance = $uibModal.open({
                templateUrl: "Front-End/app-data/views/device/device-notification.html",
                controller: "DeviceNotificationCtrl",
                size: "md",
                keyboard: false,
                backdrop: "static",
                resolve: {
                    item: function () {
                        return item;
                    },
                    option: function () {
                        return [{ Type: "noti" }];
                    }
                }
            });

            modalInstance.result.then(
                function (response) {
                    if (response.Code === 1) {
                        $notifications.success("Báo cáo thành công");
                        initData();
                    } else {
                        $notifications.warning("Có lỗi xảy ra");
                    }
                    $scope.Button.Delete.Visible = false;
                    initData();
                },
                function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                }
            );
        }

        initData();

    }]);