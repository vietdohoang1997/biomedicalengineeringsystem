﻿app.controller('DeviceStatusCtrl', ['$scope', 'CrudService', '$uibModal', "toastr",
    function ($scope, crudService, $uibModal, $notifications) {
        // Base Url 
        var baseUrl = '/api/devices/';
        $scope.deviceTypeID = 0;

        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page.  

        $scope.page = 0;


        


        var getDepartments = function () {
            var apiRoute = '/api/departments/';
            var promise = crudService.getAll(apiRoute);
            promise.then(function (response) {
                    $scope.DEPARTMENTS = response.data.Data;
                },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        $scope.Statuss = [
            { Status: 0, Name: "Mới 100%" },
            { Status: 1, Name: "Đang sử dụng" },
            { Status: 2, Name: "Hỏng" },
            { Status: 3, Name: "Đang sửa" },
            { Status: 4, Name: "Đã sửa" },
            { Status: 5, Name: "Chờ thanh lý" }
        ];

        function findObjectByKey(array, key, value) {
            for (var i = 0; i < array.length; i++) {
                if (array[i][key] === value) {
                    return array[i];
                }
            }
            return null;
        };


        $scope.getDepartmentName = function(id) {
            var result = findObjectByKey($scope.DEPARTMENTS, 'Id', id);
            return result.Name;
        };

        $scope.getStatus = function (id) {
            if (id === 0) {
                return "Mới 100%";
            }
            if (id === 1) {
                return "Đang sử dụng";
            }

            if (id === 2) {
                return "Hỏng";
            }

            if (id === 3) {
                return "Đang sửa";
            }

            if (id === 4) {
                return "Đã sửa";
            }

            if (id === 5)
                return "Chờ thanh lý";
        };

        var getDevices = function () {
            var apiRoute = baseUrl + "paging?pageIndex=" + ($scope.pageIndex - 1) + "&pageSize=" + $scope.pageSizeSelected;
            var promise = crudService.getAllPaging(apiRoute);
            promise.then(function (response) {
                    $scope.devices = response.data;
                    $scope.totalCount = response.data.TotalCount;
                    $scope.numPages = Math.ceil($scope.totalCount / $scope.pageSizeSelected);
                },
                function (error) {
                    console.log("Error: " + error);
                });
        };

        $scope.pageChanged = function () {
            getDepartments();
            getDevices();
        };

        $scope.changePageSize = function () {
            $scope.pageIndex = 1;
            getDepartments();
            getDevices();
        };

        var initData = function () {
            getDepartments();
            getDevices();
        }

        $scope.Search = function () {
            if ($scope.textSearch === undefined) {
                $scope.textSearch = "";
            }
            var apiRoute = baseUrl + "pagingv2?pageIndex=" + ($scope.pageIndex - 1) + "&pageSize=" + $scope.pageSizeSelected + "&departmentId=" + $scope.DepartmentId + "&statusId=" + $scope.StatusId + "&textSearch=" + $scope.textSearch ;
            var promise = crudService.getAllPaging(apiRoute);
            promise.then(function (response) {
                    $scope.devices = response.data;
                    $scope.totalCount = response.data.DataCount;
                    $scope.numPages = Math.ceil($scope.totalCount / $scope.pageSizeSelected);
                },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        $scope.Info = function (item) {
            var modalInstance = $uibModal.open({
                templateUrl: "Front-End/app-data/views/maintain-history/device-status-info.html",
                controller: "DeviceStatusInfoCtrl",
                size: "lg",
                keyboard: false,
                backdrop: "static",
                resolve: {
                    item: function () {
                        return item;
                    },
                    option: function () {
                        return [{ Type: "info" }];
                    }
                }
            });

            modalInstance.result.then(
                function (response) {
                    if (response.Code === 1) {
                        $notifications.success("Cập nhật thành công");
                    } else {
                        $notifications.warning("Có lỗi xảy ra");
                    }
                    $scope.Button.Delete.Visible = false;
                    initData();
                },
                function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                }
            );
        };

        initData();

    }]);