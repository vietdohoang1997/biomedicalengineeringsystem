﻿app.controller('DeviceStatusInfoCtrl', ["$scope", "CrudService", "$uibModalInstance", "item", "option", "toastr",
    function ($scope, crudService, $uibModalInstance, item, option, $notifications) {

        $scope.Device = item;

        var getDepartments = function () {
            var apiRoute = '/api/departments/';
            var promise = crudService.getAll(apiRoute);
            promise.then(function (response) {
                $scope.DEPARTMENTS = response.data.Data;
            },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        var getProviders = function () {
            var apiRoute = '/api/providers/';
            var promise = crudService.getAll(apiRoute);
            promise.then(function (response) {
                $scope.PROVIDERS = response.data.Data;
            },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        var getDeviceTypes = function () {
            var apiRoute = '/api/device_types/';
            var promise = crudService.getAll(apiRoute);
            promise.then(function (response) {
                $scope.DEVICETYPES = response.data.Data;
            },
                function (error) {
                    console.log("Error: " + error);
                });
        }




        $scope.cancel = function () {
            $uibModalInstance.dismiss("Cancel");
            console.log("hihi");
        }

        const initMain = function () {

            $scope.Title = 'THÔNG TIN THIẾT BỊ';

            getDepartments();
            getProviders();
            getDeviceTypes();
        };

        initMain();

      

    }]);