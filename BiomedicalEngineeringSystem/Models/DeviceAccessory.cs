﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BiomedicalEngineeringSystem.Models
{
    public class DeviceAccessory
    {
        [Key]
        [Column(Order = 1)]
        public int DeviceId { get; set; }

        [Key]
        [Column(Order = 2)]
        public int AccessoryId { get; set; }

        public string Note { get; set; }

        [ForeignKey("DeviceId")]
        public Device Device { get; set; }

        [ForeignKey("AccessoryId")]
        public Accessory Accessory { get; set; }
    }
}