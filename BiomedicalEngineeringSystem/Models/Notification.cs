﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BiomedicalEngineeringSystem.Models
{
    public class Notification
    {
        [Key]
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public string Content { get; set; }
        public int? DeviceId { get; set; }

        public int UserId { get; set; }
 
    }
}