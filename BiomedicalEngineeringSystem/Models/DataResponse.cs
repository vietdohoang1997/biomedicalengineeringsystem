﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiomedicalEngineeringSystem.Models
{
    public class DataResponse<T>
    {
        public int Code { get; set; }

        public string Msg { get; set; }

        public int? TotalCount { get; set; }

        public int? DataCount { get; set; } = 0;

        public T Data { get; set; }

        public static DataResponse<T> GetResult(int code, string msg, int? total, T data = default(T))
        {
            return new DataResponse<T>()
            {
                Code = code,
                Msg = msg,
                TotalCount = total,
                Data = data,
            };
        }

        public static DataResponse<T> GetResultV2(int code, string msg, int? total, int? dataCount, T data = default(T))
        {
            return new DataResponse<T>()
            {
                Code = code,
                Msg = msg,
                TotalCount = total,
                DataCount = dataCount,
                Data = data,
            };
        }
    }
}