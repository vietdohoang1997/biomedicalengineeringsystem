﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BiomedicalEngineeringSystem.Models
{
    public class SystemDBContext : DbContext
    {
        public SystemDBContext() : base("DefaultConnection")
        {
            
        }

        public virtual DbSet<Accessory> Accessories { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Device> Devices { get; set; }
        public virtual DbSet<DeviceAccessory> DeviceAccessories { get; set; }
        public virtual DbSet<DeviceType> DeviceTypes { get; set; }
        public virtual DbSet<MaintainSchedule> MaintainSchedules { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<Provider> Providers { get; set; }
        public virtual DbSet<User> Users { get; set; }
    }
}