﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BiomedicalEngineeringSystem.Models
{
    public class Accessory
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int? Size { get; set; }
        public int? Number { get; set; }
        public DateTime? ImportedDate { get; set; }
        public int Status { get; set; }

        public int ProviderId { get; set; }

        public virtual ICollection<DeviceAccessory> DeviceAccessories { get; set; }

    }
}