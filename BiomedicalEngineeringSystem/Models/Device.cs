﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BiomedicalEngineeringSystem.Models
{
    public class Device
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Model { get; set; }
        public string Serial { get; set; }
        public string Country { get; set; }
        public string Manufacture { get; set; }
        public DateTime? ProduceDate { get; set; }
        public DateTime? ImportedDate { get; set; }
        public DateTime? HandoverDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public int? Status { get; set; }
        public decimal? Price { get; set; }
        public string Note { get; set; }

        public int? ProviderId { get; set; }

        public int? TypeId { get; set; }
        [ForeignKey("TypeId")]
        public virtual DeviceType DeviceType { get; set; }

        public virtual ICollection<DeviceAccessory> DeviceAccessories { get; set; }

        public int? DepartmentId { get; set; }
      

    }

}