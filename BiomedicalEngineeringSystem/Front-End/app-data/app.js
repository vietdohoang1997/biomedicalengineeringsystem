﻿var app;
(function () {
    'use strict'; //Defines that JavaScript code should be executed in "strict mode"
    app = angular.module('myapp', ['ui.router', 'toastr', 'ngBootbox', 'ngCkeditor', 'ui.bootstrap']);
})();