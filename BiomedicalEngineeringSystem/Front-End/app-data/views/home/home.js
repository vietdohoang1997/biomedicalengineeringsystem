﻿app.controller('HomeCtrl', ['$scope', 'CrudService', '$uibModal', "toastr",
    function ($scope, crudService, $uibModal, $notifications) {
        // Base Url 
 
        $scope.FullName = localStorage.getItem("fullName");
        $scope.UserName = localStorage.getItem("userName");
        $scope.DepartmentId = localStorage.getItem("departmentId");
        $scope.RoleId = localStorage.getItem("roleId");

        $scope.Profile = function() {
            var modalInstance = $uibModal.open({
                templateUrl: "Front-End/app-data/views/home/profile.html",
                controller: "ProfileCtrl",
                size: "md",
                resolve: {
                    item: function () {
                        var obj = {};
                        obj.FullName = $scope.FullName;
                        obj.UserName = $scope.UserName;
                        return obj;
                    },
                    option: function () {
                        return [{ Type: "add" }];
                    }
                }
            });
            modalInstance.result.then(
                function (response) {
                    if (response.Code === 1) {
                        $notifications.success("Cập nhật thành công", "Thông báo");
                        initData();
                    }
                },
                function () {
                    // $log.info('Modal dismissed at: ' + new Date());
                }
            );
        }

    }]);