﻿app.controller('NewScheduleModalCtrl', ["$scope", "CrudService", "$uibModalInstance", "item", "option", "toastr",
    function ($scope, crudService, $uibModalInstance, item, option, $notifications) {

        var baseUrl = '/api/maintain_schedules/';
        $scope.Maintain = item;

        $scope.statuss = [
            { name: "Hoạt động bình thường", status: 1 },
            { name: "Đã tiếp nhận", status: 2 },
            { name: "Đã sửa", status: 3 },
            { name: "Chờ hủy ", status: 4 },
            { name: "Thanh lý", status: 5 }
        ];

        $scope.save = function () {
            var apiRoute = baseUrl;
            var postData = $scope.Maintain;
            var promise = crudService.post(apiRoute, postData);
            promise.then(function (response) {
                var dataResult = response.data;
                $uibModalInstance.close(dataResult);
            },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;
        }


        $scope.cancel = function () {
            $uibModalInstance.dismiss("Cancel");
            console.log("hihi");
        }

        const initMain = function () {
            $scope.Title = 'THÊM LỊCH SỬA CHỮA';
        };

        initMain();

    }]);