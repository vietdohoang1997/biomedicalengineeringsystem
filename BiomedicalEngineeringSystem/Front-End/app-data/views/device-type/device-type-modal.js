﻿app.controller('DeviceTypeModalCtrl', ["$scope", "CrudService", "$uibModalInstance", "item", "option", "toastr",
    function ($scope, crudService, $uibModalInstance, item, option, $notifications) {

        var baseUrl = '/api/device_types/';
        $scope.DeviceType = item;


        var addDeviceType = function () {
            var apiRoute = baseUrl;
            var postData = $scope.DeviceType;
            var promise = crudService.post(apiRoute, postData);
            promise.then(function (response) {
                var dataResult = response.data;
                $uibModalInstance.close(dataResult);
            },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;
        }

        var updateDeviceType = function () {
            var apiRoute = baseUrl + "/" + $scope.DeviceType.Id;
            var postData = $scope.DeviceType;
            var promise = crudService.put(apiRoute, postData);
            promise.then(function (response) {
                var dataResult = response.data;
                $uibModalInstance.close(dataResult);
            },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;
        }

        $scope.save = function () {
            if (option[0].Type === 'add') {
                addDeviceType();
            }
            if (option[0].Type === 'edit') {
                updateDeviceType();
            }
        }


        $scope.cancel = function () {
            $uibModalInstance.dismiss("Cancel");    
            console.log("hihi");
        }

        const initMain = function () {

            if (option[0].Type === 'edit') {
                $scope.Title = 'CẬP NHẬT LOẠI THIẾT BỊ';
            } else if (option[0].Type === 'add') {
                $scope.Title = 'THÊM MỚI LOẠI THIẾT BỊ';
            }

        };

        initMain();

    }]);