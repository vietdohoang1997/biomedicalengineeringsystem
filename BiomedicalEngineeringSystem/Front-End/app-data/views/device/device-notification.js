﻿app.controller('DeviceNotificationCtrl', ["$scope", "CrudService", "$uibModalInstance", "item", "option", "toastr",
    function ($scope, crudService, $uibModalInstance, item, option, $notifications) {

        var baseUrl = '/api/notifications/';
        $scope.Device = item;
        $scope.Noti = {};

        $scope.Noti.DeviceId = $scope.Device.Id;

        $scope.save = function () {
            var apiRoute = baseUrl;
            var postData = $scope.Noti;
            var promise = crudService.post(apiRoute, postData);
            promise.then(function (response) {
                    var dataResult = response.data;
                    $uibModalInstance.close(dataResult);
                },
                function (error) {
                    console.log("Error: " + error);
                });
            return promise;   
        }


        $scope.cancel = function () {
            $uibModalInstance.dismiss("Cancel");
            console.log("hihi");
        }

        const initMain = function () {

           $scope.Title = 'THÔNG BÁO THIẾT BỊ HỎNG';

        };

        initMain();

    }]);