﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiomedicalEngineeringSystem.DTOs
{
    public class ProviderDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Representer { get; set; }
        public int Mobile { get; set; }
        public string Email { get; set; }
    }
}