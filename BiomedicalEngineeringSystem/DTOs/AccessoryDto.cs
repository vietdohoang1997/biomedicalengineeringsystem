﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiomedicalEngineeringSystem.DTOs
{
    public class AccessoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Size { get; set; }
        public int Number { get; set; }
        public DateTime? ImportedDate { get; set; }
        public int Status { get; set; }

        public int ProviderId { get; set; }
        public ProviderDto Provider { get; set; }

        public ICollection<DeviceAccessoryDto> DeviceAccessories { get; set; }

    }
}