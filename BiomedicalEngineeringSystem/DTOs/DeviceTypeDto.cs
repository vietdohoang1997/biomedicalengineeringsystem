﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiomedicalEngineeringSystem.DTOs
{
    public class DeviceTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DeviceGroup { get; set; }
    }
}
