namespace BiomedicalEngineeringSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserNameToMaintainTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MaintainSchedules", "UserId", "dbo.Users");
            DropIndex("dbo.MaintainSchedules", new[] { "UserId" });
            AddColumn("dbo.MaintainSchedules", "UserName", c => c.String());
            AlterColumn("dbo.MaintainSchedules", "UserId", c => c.Int());
            CreateIndex("dbo.MaintainSchedules", "UserId");
            AddForeignKey("dbo.MaintainSchedules", "UserId", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MaintainSchedules", "UserId", "dbo.Users");
            DropIndex("dbo.MaintainSchedules", new[] { "UserId" });
            AlterColumn("dbo.MaintainSchedules", "UserId", c => c.Int(nullable: false));
            DropColumn("dbo.MaintainSchedules", "UserName");
            CreateIndex("dbo.MaintainSchedules", "UserId");
            AddForeignKey("dbo.MaintainSchedules", "UserId", "dbo.Users", "Id", cascadeDelete: true);
        }
    }
}
