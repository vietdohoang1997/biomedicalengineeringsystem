namespace BiomedicalEngineeringSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeForeinKey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Devices", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.Devices", "ProviderId", "dbo.Providers");
            DropForeignKey("dbo.Accessories", "ProviderId", "dbo.Providers");
            DropForeignKey("dbo.MaintainSchedules", "DeviceId", "dbo.Devices");
            DropForeignKey("dbo.Users", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.MaintainSchedules", "UserId", "dbo.Users");
            DropForeignKey("dbo.Notifications", "UserId", "dbo.Users");
            DropIndex("dbo.Accessories", new[] { "ProviderId" });
            DropIndex("dbo.Devices", new[] { "ProviderId" });
            DropIndex("dbo.Devices", new[] { "DepartmentId" });
            DropIndex("dbo.MaintainSchedules", new[] { "DeviceId" });
            DropIndex("dbo.MaintainSchedules", new[] { "UserId" });
            DropIndex("dbo.Users", new[] { "DepartmentId" });
            DropIndex("dbo.Notifications", new[] { "UserId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Notifications", "UserId");
            CreateIndex("dbo.Users", "DepartmentId");
            CreateIndex("dbo.MaintainSchedules", "UserId");
            CreateIndex("dbo.MaintainSchedules", "DeviceId");
            CreateIndex("dbo.Devices", "DepartmentId");
            CreateIndex("dbo.Devices", "ProviderId");
            CreateIndex("dbo.Accessories", "ProviderId");
            AddForeignKey("dbo.Notifications", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.MaintainSchedules", "UserId", "dbo.Users", "Id");
            AddForeignKey("dbo.Users", "DepartmentId", "dbo.Departments", "Id", cascadeDelete: true);
            AddForeignKey("dbo.MaintainSchedules", "DeviceId", "dbo.Devices", "Id");
            AddForeignKey("dbo.Accessories", "ProviderId", "dbo.Providers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Devices", "ProviderId", "dbo.Providers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Devices", "DepartmentId", "dbo.Departments", "Id", cascadeDelete: true);
        }
    }
}
