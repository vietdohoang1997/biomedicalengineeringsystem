namespace BiomedicalEngineeringSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDeviceNameAndDepartmenentToMaintainSchedule : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MaintainSchedules", "DeviceId", "dbo.Devices");
            DropIndex("dbo.MaintainSchedules", new[] { "DeviceId" });
            AddColumn("dbo.MaintainSchedules", "DeviceName", c => c.String());
            AddColumn("dbo.MaintainSchedules", "DepartmentName", c => c.String());
            AlterColumn("dbo.MaintainSchedules", "DeviceId", c => c.Int());
            CreateIndex("dbo.MaintainSchedules", "DeviceId");
            AddForeignKey("dbo.MaintainSchedules", "DeviceId", "dbo.Devices", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MaintainSchedules", "DeviceId", "dbo.Devices");
            DropIndex("dbo.MaintainSchedules", new[] { "DeviceId" });
            AlterColumn("dbo.MaintainSchedules", "DeviceId", c => c.Int(nullable: false));
            DropColumn("dbo.MaintainSchedules", "DepartmentName");
            DropColumn("dbo.MaintainSchedules", "DeviceName");
            CreateIndex("dbo.MaintainSchedules", "DeviceId");
            AddForeignKey("dbo.MaintainSchedules", "DeviceId", "dbo.Devices", "Id", cascadeDelete: true);
        }
    }
}
