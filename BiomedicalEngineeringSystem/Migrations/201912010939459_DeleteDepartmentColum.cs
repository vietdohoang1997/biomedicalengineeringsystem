namespace BiomedicalEngineeringSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteDepartmentColum : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.MaintainSchedules", "DepartmentName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MaintainSchedules", "DepartmentName", c => c.String());
        }
    }
}
