﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BiomedicalEngineeringSystem.DTOs;
using BiomedicalEngineeringSystem.Models;
using Microsoft.Ajax.Utilities;

namespace BiomedicalEngineeringSystem.Controllers
{
    public class NotificationsController : ApiController
    {
        private SystemDBContext _context;

        public NotificationsController()
        {
            _context = new SystemDBContext();
        }

        [HttpGet]
        [Route("api/notifications")]
        public DataResponse<List<Notification>> GetAllNotifications()
        {
            var deviceQuery = _context.Notifications.AsQueryable();

            return DataResponse<List<Notification>>.GetResult(1, "OK", deviceQuery.ToList().Count(), deviceQuery.ToList());
        }

        [HttpGet]
        [Route("api/notifications/{id:int}")]
        public DataResponse<Notification> GetNotificationById(int id)
        {
            var deviceQuery = _context.Notifications.SingleOrDefault(d => d.Id == id);

            if (deviceQuery == null)
                return DataResponse<Notification>.GetResult(-1, "Not Found", null, null);

            return DataResponse<Notification>.GetResult(1, "OK", 1, deviceQuery);
        }

        [HttpGet]
        [Route("api/notifications/paging")]
        public DataResponse<List<NotificationDto>> GetDepartmentByPaging(int pageIndex, int pageSize, string textSearch = "")
        {
            var datas = (from n in _context.Notifications.ToList()
                    join d in _context.Devices.ToList()
                        on n.DeviceId equals d.Id
                    join dept in _context.Departments.ToList()
                        on d.DepartmentId equals dept.Id
                    select new NotificationDto()
                    {
                        Id = n.Id,
                        DeviceName = d.Name,
                        Department = dept.Name,
                        Time = n.Time,
                        Content = n.Content,
                        UserId = n.UserId
                    }
                ).OrderByDescending(n => n.Time).DistinctBy(n => n.DeviceName).ToList();

            return DataResponse<List<NotificationDto>>.GetResultV2(1, "OK", datas.Count, datas.Count, datas);
        }

        [HttpPost]
        [Route("api/notifications")]
        // GET api/<controller>
        public DataResponse<Notification> CreateNewNotification(Notification newNotification)
        {

            if (!ModelState.IsValid)
                return DataResponse<Notification>.GetResult(-1, "Model is not valid", null, null);

            newNotification.Time = DateTime.Now;
            newNotification.UserId = 1;

            var deviceInDb = _context.Devices.SingleOrDefault(d => d.Id == newNotification.DeviceId);
            if (deviceInDb != null)
            {
                deviceInDb.Status = 2;
            }

            _context.Notifications.Add(newNotification);

            _context.SaveChanges();

            return DataResponse<Notification>.GetResult(1, "Created", 1, newNotification);
        }


        [HttpPut]
        [Route("api/notifications/{id:int}")]
        public DataResponse<Notification> UpdateNotification(int id, Notification updateNotification)
        {
            if (!ModelState.IsValid)
                return DataResponse<Notification>.GetResult(-1, "Model is not valid", null, null);

            var deviceInDb = _context.Notifications.SingleOrDefault(d => d.Id == id);

            if (deviceInDb == null)
                return DataResponse<Notification>.GetResult(-1, "Not Found", null, null);

            deviceInDb.Time = updateNotification.Time;
            deviceInDb.Content = updateNotification.Content;
            deviceInDb.UserId = updateNotification.UserId;

            _context.SaveChanges();

            return DataResponse<Notification>.GetResult(1, "Updated", 1, updateNotification);
        }

        [HttpDelete]
        [Route("api/notifications/{id:int}")]
        public DataResponse<Notification> DeleteNotification(int id)
        {
            var deviceQuery = _context.Notifications.SingleOrDefault(d => d.Id == id);

            if (deviceQuery == null)
                return DataResponse<Notification>.GetResult(-1, "Not Found", null, null);

            _context.Notifications.Remove(deviceQuery);

            _context.SaveChanges();

            return DataResponse<Notification>.GetResult(1, "Deleted", null);
        }
    }
}
