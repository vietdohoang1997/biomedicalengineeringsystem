﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BiomedicalEngineeringSystem.Models;

namespace BiomedicalEngineeringSystem.Controllers
{
    public class ProvidersController : ApiController
    {
        private SystemDBContext _context;

        public ProvidersController()
        {
            _context = new SystemDBContext();
        }

        [HttpGet]
        [Route("api/providers")]
        public DataResponse<List<Provider>> GetAllProviders(string query = null)
        {
            var deviceQuery = _context.Providers.AsQueryable();

            if (!String.IsNullOrWhiteSpace(query))
                deviceQuery = deviceQuery.Where(c => c.Name.Contains(query));


            return DataResponse<List<Provider>>.GetResult(1, "OK", deviceQuery.ToList().Count(), deviceQuery.ToList());
        }

        [HttpGet]
        [Route("api/providers/{id:int}")]
        public DataResponse<Provider> GetProviderById(int id)
        {
            var deviceQuery = _context.Providers.SingleOrDefault(d => d.Id == id);

            if (deviceQuery == null)
                return DataResponse<Provider>.GetResult(-1, "Not Found", null, null);

            return DataResponse<Provider>.GetResult(1, "OK", 1, deviceQuery);
        }

        [HttpGet]
        [Route("api/providers/paging")]
        public DataResponse<List<Provider>> GetProviderByPaging(int pageIndex, int pageSize, string textSearch = "")
        {
            var total = _context.Providers.ToList();

            var providerQuery = _context.Providers.Where(d => d.Name.Contains(textSearch)).OrderBy(d => d.Id).Skip(pageIndex * pageSize).Take(pageSize).ToList();

            if (providerQuery.Count == 0)
                return DataResponse<List<Provider>>.GetResult(-1, "Not Found", null);

            return DataResponse<List<Provider>>.GetResultV2(1, "OK", total.Count, providerQuery.Count, providerQuery);
        }

        [HttpPost]
        [Route("api/providers")]
        // GET api/<controller>
        public DataResponse<Provider> CreateNewProvider(Provider newProvider)
        {

            if (!ModelState.IsValid)
                return DataResponse<Provider>.GetResult(-1, "Model is not valid", null, null);

            _context.Providers.Add(newProvider);
            _context.SaveChanges();

            return DataResponse<Provider>.GetResult(1, "Created", 1, newProvider);
        }


        [HttpPut]
        [Route("api/providers/{id:int}")]
        public DataResponse<Provider> UpdateProvider(int id, Provider updateProvider)
        {
            if (!ModelState.IsValid)
                return DataResponse<Provider>.GetResult(-1, "Model is not valid", null, null);

            var deviceInDb = _context.Providers.SingleOrDefault(d => d.Id == id);

            if (deviceInDb == null)
                return DataResponse<Provider>.GetResult(-1, "Not Found", null, null);

            deviceInDb.Name = updateProvider.Name;
            deviceInDb.Address = updateProvider.Address;
            deviceInDb.Representer = updateProvider.Representer;
            deviceInDb.Mobile = updateProvider.Mobile;
            deviceInDb.Email = updateProvider.Email;

            _context.SaveChanges();

            return DataResponse<Provider>.GetResult(1, "Updated", 1, updateProvider);
        }

        [HttpDelete]
        [Route("api/providers/{id:int}")]
        public DataResponse<Provider> DeleteProvider(int id)
        {
            var deviceQuery = _context.Providers.SingleOrDefault(d => d.Id == id);

            if (deviceQuery == null)
                return DataResponse<Provider>.GetResult(-1, "Not Found", null, null);

            _context.Providers.Remove(deviceQuery);

            _context.SaveChanges();

            return DataResponse<Provider>.GetResult(1, "Deleted", null);
        }
    }
}
