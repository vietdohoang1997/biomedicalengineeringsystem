﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BiomedicalEngineeringSystem.Models;

namespace BiomedicalEngineeringSystem.Controllers
{
    public class UsersController : ApiController
    {
        private SystemDBContext _context;

        public UsersController()
        {
            _context = new SystemDBContext();
        }

        [HttpGet]
        [Route("api/users")]
        public DataResponse<List<User>> GetAllUsers(string query = null)
        {
            var userQuery = _context.Users.AsQueryable();

            if (!String.IsNullOrWhiteSpace(query))
                userQuery = userQuery.Where(c => c.FullName.Contains(query));


            return DataResponse<List<User>>.GetResult(1, "OK", userQuery.ToList().Count(), userQuery.ToList());
        }

        [HttpGet]
        [Route("api/users/{id:int}")]
        public DataResponse<User> GetUserById(int id)
        {
            var userQuery = _context.Users.SingleOrDefault(d => d.Id == id);

            if (userQuery == null)
                return DataResponse<User>.GetResult(-1, "Not Found", null, null);

            return DataResponse<User>.GetResult(1, "OK", 1, userQuery);
        }

        [HttpGet]
        [Route("api/users/login")]
        public DataResponse<User> Login(string username, string password)
        {
            var userQuery = _context.Users.SingleOrDefault(d => d.UserName == username && d.Password == password);
         
            if (userQuery == null)
                return DataResponse<User>.GetResult(-1, "Not Found", null, null);

            return DataResponse<User>.GetResult(1, "OK", 1, userQuery);
        }

        [HttpGet]
        [Route("api/users/paging")]
        public DataResponse<List<User>> GetDepartmentByPaging(int pageIndex, int pageSize, string textSearch = "")
        {
            var total = _context.Users.ToList();

            var departmentQuery = _context.Users.Where(d => d.FullName.Contains(textSearch)).OrderBy(d => d.Id).Skip(pageIndex * pageSize).Take(pageSize).ToList();

            if (departmentQuery.Count == 0)
                return DataResponse<List<User>>.GetResult(-1, "Not Found", null);

            return DataResponse<List<User>>.GetResultV2(1, "OK", total.Count, departmentQuery.Count, departmentQuery);
        }

        [HttpPost]
        [Route("api/users")]
        // GET api/<controller>
        public DataResponse<User> CreateNewUser(User newUser)
        {

            if (!ModelState.IsValid)
                return DataResponse<User>.GetResult(-1, "Model is not valid", null, null);

            _context.Users.Add(newUser);
            _context.SaveChanges();

            return DataResponse<User>.GetResult(1, "Created", 1, newUser);
        }


        [HttpPut]
        [Route("api/users/{id:int}")]
        public DataResponse<User> UpdateUser(int id, User updateUser)
        {
            if (!ModelState.IsValid)
                return DataResponse<User>.GetResult(-1, "Model is not valid", null, null);

            var userInDb = _context.Users.SingleOrDefault(d => d.Id == id);

            if (userInDb == null)
                return DataResponse<User>.GetResult(-1, "Not Found", null, null);

            userInDb.FullName = updateUser.FullName;
            userInDb.UserName = updateUser.UserName;
            userInDb.Password = updateUser.Password;
            userInDb.Email = updateUser.Email;
            userInDb.Mobile = updateUser.Mobile;
            userInDb.Address = updateUser.Address;
            userInDb.Role = updateUser.Role;
            userInDb.DepartmentId = updateUser.DepartmentId;

            _context.SaveChanges();

            return DataResponse<User>.GetResult(1, "Updated", 1, updateUser);
        }

        [HttpDelete]
        [Route("api/users/{id:int}")]
        public DataResponse<User> DeleteUser(int id)
        {
            var userQuery = _context.Users.SingleOrDefault(d => d.Id == id);

            if (userQuery == null)
                return DataResponse<User>.GetResult(-1, "Not Found", null, null);

            _context.Users.Remove(userQuery);

            _context.SaveChanges();

            return DataResponse<User>.GetResult(1, "Deleted", null);
        }
    }
}
