﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BiomedicalEngineeringSystem.Models;

namespace BiomedicalEngineeringSystem.Controllers
{
    public class DeviceTypesController : ApiController
    {
        private SystemDBContext _context;

        public DeviceTypesController()
        {
            _context = new SystemDBContext();
        }

        [HttpGet]
        [Route("api/device_types")]
        public DataResponse<List<DeviceType>> GetAllDeviceTypes(string query = null)
        {
            var deviceQuery = _context.DeviceTypes.AsQueryable();

            if (!String.IsNullOrWhiteSpace(query))
                deviceQuery = deviceQuery.Where(c => c.Name.Contains(query));


            return DataResponse<List<DeviceType>>.GetResult(1, "OK", deviceQuery.ToList().Count(), deviceQuery.ToList());
        }

        [HttpGet]
        [Route("api/device_types/{id:int}")]
        public DataResponse<DeviceType> GetDeviceTypeById(int id)
        {
            var deviceQuery = _context.DeviceTypes.SingleOrDefault(d => d.Id == id);

            if (deviceQuery == null)
                return DataResponse<DeviceType>.GetResult(-1, "Not Found", null, null);

            return DataResponse<DeviceType>.GetResult(1, "OK", 1, deviceQuery);
        }

        [HttpGet]
        [Route("api/device_types/paging")]
        public DataResponse<List<DeviceType>> GetDeviceTypeByPaging(int pageIndex, int pageSize, string textSearch = "")
        {
            var total = _context.DeviceTypes.ToList();

            var deviceTypeQuery = _context.DeviceTypes.Where(d => d.Name.Contains(textSearch)).OrderBy(d => d.Id).Skip(pageIndex * pageSize).Take(pageSize).ToList();

            if (deviceTypeQuery.Count == 0)
                return DataResponse<List<DeviceType>>.GetResult(-1, "Not Found", null);

            return DataResponse<List<DeviceType>>.GetResultV2(1, "OK", total.Count, deviceTypeQuery.Count, deviceTypeQuery);
        }

        [HttpPost]
        [Route("api/device_types")]
        // GET api/<controller>
        public DataResponse<DeviceType> CreateNewDevice(DeviceType newDevice)
        {

            if (!ModelState.IsValid)
                return DataResponse<DeviceType>.GetResult(-1, "Model is not valid", null, null);

            _context.DeviceTypes.Add(newDevice);
            _context.SaveChanges();

            return DataResponse<DeviceType>.GetResult(1, "Created",1, newDevice);
        }


        [HttpPut]
        [Route("api/device_types/{id:int}")]
        public DataResponse<DeviceType> UpdateDeviceType(int id, DeviceType updateDeviceType)
        {
            if (!ModelState.IsValid)
                return DataResponse<DeviceType>.GetResult(-1, "Model is not valid", null, null);

            var deviceInDb = _context.DeviceTypes.SingleOrDefault(d => d.Id == id);

            if (deviceInDb == null)
                return DataResponse<DeviceType>.GetResult(-1, "Not Found", null, null);

            deviceInDb.Name = updateDeviceType.Name;
            deviceInDb.DeviceGroup = updateDeviceType.DeviceGroup;

            _context.SaveChanges();

            return DataResponse<DeviceType>.GetResult(1, "Updated", 1, updateDeviceType);
        }

        [HttpDelete]
        [Route("api/device_types/{id:int}")]
        public DataResponse<DeviceType> DeleteDeviceType(int id)
        {
            var deviceQuery = _context.DeviceTypes.SingleOrDefault(d => d.Id == id);

            if (deviceQuery == null)
                return DataResponse<DeviceType>.GetResult(-1, "Not Found", null, null);

            _context.DeviceTypes.Remove(deviceQuery);

            _context.SaveChanges();

            return DataResponse<DeviceType>.GetResult(1, "Deleted", null);
        }

    }
}
