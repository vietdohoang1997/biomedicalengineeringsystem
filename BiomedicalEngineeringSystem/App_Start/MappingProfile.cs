﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using BiomedicalEngineeringSystem.DTOs;
using BiomedicalEngineeringSystem.Models;

namespace BiomedicalEngineeringSystem.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<Device, DeviceDto>();
            Mapper.CreateMap<DeviceDto, Device>();
        }
    }
}